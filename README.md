# Vänsterpartiets partiprogrampodden

This is a simple project to create a podcast (or audiobook?) for [Vänsterpartiets partiprogram](https://www.vansterpartiet.se/resursbank/partiprogram/).

We use [piper](https://github.com/rhasspy/piper) to do text to speech. So download a binary release and the Swedish voice and config described in the repo.

Navigate to the directory where you have installed piper. (Make sure the relative paths)

Run the following to to text to speech:

```
../../code/vpartiprogrampodden.gitlab.io/do-text-to-speech.sh
```

We will use [python-feedgen](https://github.com/lkiesow/python-feedgen) to generate the RSS feed.

```
virtualenv -p python3 venv
source venv/bin/activate
pip install feedgen
python3 generate_rss.py
```

Test with builtin python web server:

```
python3 -m http.server
```

