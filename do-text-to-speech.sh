#!/bin/sh

IFS='
'

program_dir=../../code/vpartiprogrampodden.gitlab.io/partiprogramtext
audio_dir=audio

mkdir -p ${audio_dir}
for filename in `ls ${program_dir}`; do
    filename_no_ext="${filename%.txt}"
    echo ${filename_no_ext}
    # Convert text to wav
    cat "${program_dir}/${filename}" | ./piper --model sv_SE-nst-medium.onnx --output_file "${audio_dir}/${filename_no_ext}.wav"
    # Convert wav to mp3
    ffmpeg -i "${audio_dir}/${filename_no_ext}.wav" -acodec libmp3lame "${audio_dir}/${filename_no_ext}.mp3"
done