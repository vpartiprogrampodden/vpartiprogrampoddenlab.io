Vänsterpartiets grundsyn i sakfrågor.

Stärk den politiska demokratin.

Vi vill bygga vidare på Sveriges demokratisering. Vi vill förstärka och fördjupa de politiska fri- och rättigheter folket drivit igenom i konflikt med en konservativ överhet. Den demokratiska principen om en person en röst står i skarp kontrast till den makt som grundas i ägandet av banker och kapital. FN:s allmänna förklaring om de mänskliga rättigheterna var ett historiskt framsteg vi vill förverkliga fullt ut.

Folkets politiska makt över staten ska utövas av riksdagen, tillsatt genom allmänna, fria och rättvisa val, där mandaten fördelas proportionellt mellan partier. Regeringen ska vara ansvarig inför riksdagen. Vi vill se en republikansk författning där statschefen tillsätts i demokratisk ordning.

Den offentliga maktutövningen ska vägledas av alla människors lika värde. Grundlagarna ska värna demokratin och människors fri- och rättigheter. Några av den politiska demokratins viktigaste byggstenar är föreningsfriheten, yttrande- och tryckfriheten, demonstrationsfriheten, meddelarfriheten och offentlighetsprincipen. Alla människor har rätt till skydd av sin personliga integritet. Religionsfriheten ska garanteras och staten ska vara sekulär, det vill säga inte ha några band till någon religion.

Samhället behöver ett väl fungerande rättsväsende, med självständiga och opolitiska domstolar där det finns ett tydligt lekmannainflytande. Utvecklingen av ett rättsväsende byggt på rättssäkerhet är viktig att försvara och fortsätta förverkliga. Principen att människor ska anses oskyldiga tills motsatsen är bevisad är grundläggande i det. Rättssäkerheten behöver upprätthållas genom rättshjälp, medborgerlig insyn och oberoende granskning.

Rättsväsendet har historiska problem som kommer ur klassamhället, patriarkatet och rasistiskt tankegods och behöver ett aktivt och medvetet arbete för att lägga det bakom sig. Det gör det särskilt viktigt att säkra en folklig förankring i rättsprocesserna. Polisen och Försvarsmakten spelar roller i samhället som innebär att de behöver vara tydligt inordnade i demokratiska strukturer. Det ställer också höga krav på fungerande ansvarsutkrävande vid lagbrott.

Det kommunala självstyret är en betydelsefull del av den svenska demokratin. Rollfördelningen mellan staten, regionerna och kommunerna behöver balansera värdet i att fatta beslut nära medborgarna med möjligheterna att bygga ett sammanhållet land. Staten har det övergripande ansvaret för välfärden. Det gör det möjligt att omfördela resurser på ett rättvist sätt genom det progressiva skattesystemet. Inkomster och kostnader behöver fördelas solidariskt över landet. Skola, vård, omsorg och annan service behöver fungera överallt.