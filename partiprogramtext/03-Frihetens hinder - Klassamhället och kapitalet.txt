Frihetens hinder.

Klassamhället och kapitalet.

Den värld vi lever i har en lång historia av klassamhällen. Människor har tvingats arbeta hårt för att ett fåtal furstar, slavägare och patroner ska kunna bygga upp makt och ägande. Den kedjan av olika klassamhällen fortsätter hela vägen till idag. Många av dagens miljardärer sitter på förmögenheter som grundlades långt innan de demokratiska genombrotten.

Med industrisamhällets framväxt förändrades klassamhällena dramatiskt. Arbetet blev långt mer produktivt. Ägandet började organiseras i bolag som drevs att ständigt försöka sänka sina kostnader och vara steget före varandra. Den kapitalistiska dynamiken formade mer och mer av samhället.

Det lade grunden för två nya samhällsklasser: en klass av kapitalägare och en klass av arbetare. När människor knöts ihop kring den storskaliga produktionen i städerna öppnades helt nya möjligheter till organisering. I land efter land växte starka arbetarrörelser fram, tillsammans med en uppsjö av andra folkliga rörelser. De drev igenom en lång rad krav på demokrati, rättigheter och socialistiska lösningar trots hårt motstånd från överklassen. För första gången kunde vanliga människor organisera sig så brett att de kunde göra anspråk på att själva leda samhället.

Många politiska motsättningar handlar i grunden om intresse­konflikten mellan alla oss som arbetar och skapar värde, och å andra sidan det fåtal som äger de stora företagen. Hur mycket av det värde vi skapar de tar i vinst och hur mycket som går tillbaka till oss är en fråga om makt.

Deras behov av att höja sina vinstmarginaler och bygga upp sina maktpositioner ger dem starka intressen i hur hela samhället är uppbyggt. Det har format mycket av den moderna historien.
Alla politiska rörelser som vill förbättra människors livsvillkor krockar på ett eller annat sätt med kapitalismens dynamik. Det gör att vitt skilda sakfrågor knyts samman i ett gemensamt intresse av att stärka de demokratiska krafterna och göra bättre vägval möjliga.

Överklassen är beroende av det arbete vi utför, och det ger oss förhandlingsstyrka. Det ligger i deras intresse att försöka hålla tillbaka den. Arbetarrörelsens uppgift är tvärtom att organisera den till en kraft som kan förändra samhället.

Låsningar på arbetsplatserna.

En arbetsplats där människor har bra sammanhållning, löser problem gemensamt och känner sig trygga att säga vad de tycker är en produktiv arbetsplats. Det är också en arbetsplats där människor kommer att ha lätt att bygga upp förhandlingsstyrka tillsammans.

Just därför driver ägarnas intresse av att hålla nere lönerna ofta fram dåliga företagskulturer. Utbytbarhet och kontroll sänker människors förhandlingsstyrka och pressar fram hårdare arbetsdagar. Individuella löner ställer människor mot varandra. Samtidigt punkteras det engagemang som lägger grunden för att arbetsplatser och samhällen ska fungera ordentligt.

Det är ett grundläggande problem med ägandeformer som bygger på vinstmaximering. Från ett samhällsperspektiv är det en enorm förlust varje gång en människa tappar sin genuina drivkraft att göra ett gott arbete i yrkeslivet. För det enskilda företaget kan det tvärtom vara lönsamt att slita ut människor och säga upp dem istället för att lägga tid på återhämtning, kompetensutveckling och att skapa bra arbetsplatser. Vinstdrivande företag är inte gjorda för att lyfta blicken och se de större konsekvenserna av sina beslut.

Den enögdheten kan driva dem att förstöra biologisk mångfald och ekosystem, vars långsiktiga bärkraft betyder lite för deras lönsamhet men mycket för framtida generationers liv på denna planet. Den får dem att låsa in kunskap som skulle göra långt större nytta om den kunde användas fritt. Den leder dem till att slösa enorma resurser på konflikter med varandra i form av allt från marknadsföring till juridiska tvister.

En ekonomi där de här tendenserna är utbredda leder till ett samhälle som hela tiden tvingas hanka sig fram genom små och stora kriser. Med gemensamma och demokratiska ägandeformer skulle vi kunna förebygga problem innan de växer sig stora. Fler människor skulle kunna gå till jobbet med en känsla av att vara en viktig och uppskattad del av en arbetsplats världen behöver. Fler uppgifter skulle utföras av någon som är fri att göra ett bra arbete. Samhällen där människor möts med respekt på sina arbetsplatser fungerar långt bättre.

Arbetarklassen och det skiktade samhället.

Lönearbete betyder i praktiken olika saker för olika människor. Livs- och arbetsvillkoren skiljer sig drastiskt på djupt orättvisa sätt.

För arbetarklassen är lönearbetets grundvillkor att det bara är genom att sälja sitt arbete det går att försörja sig. Arbetsdagarna präglas av ett begränsat inflytande över sitt eget och andras arbete. En oorganiserad arbetarklass är enkelt utbytbar och tvingas oftare möta den kalla kärnan i kapitalets dynamik. En organiserad arbetar­klass har å andra sidan en stark förhandlingsposition gentemot kapitalet. Många har kritiska positioner i produktionskedjan eller för att samhället ska fungera.

Andra grupper av löntagare är i annorlunda positioner. Bland tjänste­personer har det funnits bättre möjligheter att säkra rimliga villkor genom individuella förhandlingar. Samtidigt har många av de yrkesgrupperna proletariserats så att villkoren mer kommit att likna arbetarklassens. Det ger bred organisering en än större betydelse. När vi står upp tillsammans, som kollektiv, är vi långt mindre utbytbara och kan ta tillbaka makten.

Ju större personliga konsekvenser det skulle få för människor att förlora jobbet, desto starkare är ägarnas övertag på arbetsplatsen. Det ger dem ett intresse av ett samhälle med djup fattigdom och utbredd ekonomisk stress. Ju tryggare och friare vi är, desto mer förhandlingsstyrka har vi.

Därför har den ägande klassen också en i grunden kluven syn på arbetslöshet. I ett samhälle med stora investeringar, där det finns gott om jobb att söka, kommer vi att kunna ställa krav på bra löner och arbetsvillkor. Att Sverige under flera årtionden drev en politik för full sysselsättning skapade en situation på arbetsplatserna som gjorde att arbetarklassen kunde ta stora steg framåt. Det kom att prägla hela samhällsutvecklingen.

Det var viktigt för kapitalägarna att bryta med det och tvärtom normalisera en hög arbetslöshet. Trots det ofattbara slöseri med människors arbetsförmåga det inneburit, har det lönat sig för dem eftersom makten på arbetsplatserna förskjutits till deras fördel.
Från deras perspektiv kan lägre produktion vara ett pris värt att betala för att säkra en mer orättvis fördelning av kakan.

Att människors fattigdom sänker förhandlingsstyrkan betyder också att kapitalet inte är särskilt intresserade av att samhället stödjer människor att ta sig ur den. När bostadsbolagen höjer hyrorna eller butikerna höjer matpriserna ökar pressen på människor att behålla sina jobb även om villkoren är orimliga. Ett samhälle där människor fastnar i djupa skulder är både lönsamt för bankerna och fördelaktigt för kapitalet i stort.

Stadsdelar, förorter, bruksorter, småorter och landsbygd lämnas med färre möjligheter till utveckling när människor görs utbytbara och arbetslösa. Det kan handla om stängda skolor och avsaknad av service, eller om en självförstärkande kombination av fattigdom, brott och skuldbeläggning. Istället för att bygga upp starka lokalsamhällen med resurser att lösa de problemen, drar sig den ekonomiska eliten hellre undan till välbärgade områden.

Överklassen har ett intresse av att stärka de skillnaderna.
De har större möjligheter att vinna stöd för sin sak i ett samhälle där chefer, politiker och andra i maktställning lever på helt andra villkor än de flesta. Sammanhållning och jämlikhet betyder tvärtom att vanliga människors livsvillkor hamnar i centrum.

Ett samhälle med djupa klasskillnader kommer inte att hänga ihop. Människor har enklare att förstå varandra, bygga förtroende och arbeta tillsammans ju mer vi kan mötas på samma villkor.

Kapitalets instabilitet.

Skiktningen av samhället är ett av flera exempel på hur kapitalismens dynamik leder till en grundläggande instabilitet i samhället och ekonomin. Vi har länge sett en ohållbar utveckling där alltmer kapital koncentrerats på allt färre händer. Det kommer att behövas aktiv politisk handling för att bryta mönstret att mer och mer makt samlas hos miljardärer, som utövar den helt godtyckligt.

I många företag märks kapitalets instabilitet när ägandet skiljs från verksamheten och besluten börjar fattas i styrelserum med väldigt svag koppling till det verkliga arbete företaget bygger på. Den syns i hur ägare kan ta stora risker och låta andra stå för notan när ett bolag förstör naturresurser eller går i konkurs. Att vinstdrivande företag ensidigt fokuserar på sin egen lönsamhet skapar en kultur av ointresse för samhällsekonomins komplexitet, som också kommit att prägla andra institutioner.

I ekonomin i stort syns den instabiliteten bland annat i djupa kriser som kan förlama många länders ekonomiska utveckling i åratal. Oförmågan att sätta människor i arbete leder till en nedåtgående spiral där många tvingas dra ner på sina utgifter och därmed driver än fler företag till uppsägningar och konkurs.

Kapitalet behöver politisk stabilitet och statliga investeringar i
till exempel utbildning för att stå starka i konkurrensen på sikt. Samtidigt kan enskilda kapitalintressen sällan hålla sig från att i varje läge driva sina ekonomiska intressen så hårt de kan.
När särintressen som skolbolagen köper politiska tjänster och sänker standarden i viktiga verksamheter blir det särskilt tydligt. Kapitalet undergräver ständigt både sina långsiktiga förutsättningar och sin egen legitimitet.

Överklassens politiska roll.

Överklassen omsätter sin ekonomiska makt i politisk makt genom tankesmedjor, lobbyister, medieägande och högerpartier. Det är en fortsättning på en lång historia av maktutövande uppifrån. Många av dagens politiska idéer och institutioner växte en gång i tiden fram utifrån intressena hos gamla tiders överhet. Det har tagit tid och kraft att förändra inriktningen på till exempel utbildnings- och rättsväsendet i en mer demokratisk riktning.

Med demokratins genombrott uppstod en grundläggande oro i
den ägande klassen för att den politiska sfären skulle ha för stort handlingsutrymme. Det har varit viktigt för dem att försöka snäva in demokratins ramar för att begränsa utrymmet för människor att driva igenom sina egna intressen. Den ekonomiska eliten behöver ett politiskt etablissemang som står nära dem och lever långt från vanliga människors vardag.

De senaste decennierna har präglats av att de förstärkt sin politiska makt och använt den till att sätta press på den välfärd arbetarrörelsen har byggt upp. Minskade resurser från staten och utförsäljning av en lång rad verksamheter har lett till en lång rad problem. Välfärden har urholkats och klyftorna i samhället har ökat. Politikens möjligheter att styra de gemensamma verksamheterna har beskurits och grov kriminalitet har letat sig in i ägarstrukturerna.

Ju mer kapitalet förmår forma samhället, desto mer kommer högerns politiska projekt att behöva handla om att hantera olika sorters kriser. Deras ideologiska behov handlar därför till stor del om att undvika konstruktiva, demokratiska processer, som skulle synliggöra hur deras misslyckade politik gång på gång krockar med större samhällsintressen.

De behöver uppgivenhet och misstroende, alarmism och syndabockar. Deras klassförakt syns i en dömande människosyn, som handlar om att människor är lata, själviska och behöver pressas att göra nytta. Tanken att varje människa är värd trygghet, frihet och jämlikhet är fortfarande radikal.

Ensidig vinstmaximering fungerar sämre och sämre i den komplexa samhällsekonomi som växer fram. När vi löser olika samhällsproblem på bättre sätt svarar det inte bara på situationen här och nu – det lägger också grunderna för ett nytt, friare och mer demokratiskt samhälle. På samma sätt som kungar och drottningar inte längre styr världen kommer också kapitalismen en dag vara historia.