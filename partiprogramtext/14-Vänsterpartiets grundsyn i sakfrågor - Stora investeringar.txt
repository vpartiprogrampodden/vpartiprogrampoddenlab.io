Stora investeringar.

Det är människors arbete som driver den ekonomiska utvecklingen. Vi vill bygga ett samhälle där det finns bra och meningsfulla jobb till alla. Omfattande investeringar för att ta itu med de stora samhällsutmaningarna bildar kärnan i den samlade strategi för full sysselsättning vi vill se. Riksbanken behöver demokratiseras och driva en penningpolitik som prioriterar sysselsättningen högt.

Vi vill att regeringen arbetar med en utsläppsbudget som knyter ihop klimatmålen med konkreta, siffersatta förslag. Det gör det möjligt att bygga upp strategier med fokus på det som är mest avgörande.

Sverige behöver också ett medvetet och heltäckande arbete med att skapa goda villkor för rika ekosystem, bland annat genom att återställa natur och kompensera för förlorade naturvärden. Våtmarker spelar en särskild roll för att sänka klimatutsläppen.
Vi vill se ambitiösa miljömål som följs upp med skarp lagstiftning och praktiskt arbete.

För att ta tag i de stora samhällsutmaningarna kommer skattenivåerna att behöva öka igen, framför allt på stora förmögenheter, fastigheter, arv och höga inkomster. Vi vill se ett enkelt, stabilt och solidariskt skattesystem där de rikaste åter börjar betala sin del. Skatterna bidrar på många sätt till ett tryggt, jämlikt och jämställt samhälle, särskilt om de utformas progressivt och används till saker alla har användning av.

Skatter kan spela en viktig kompletterande roll för att driva på klimatomställningen. De behöver utformas klokt för att få verklig effekt och vinna politiskt stöd. Kostnaderna för utsläpp behöver öka långsiktigt på ett sätt som är tryggt och förutsägbart, i takt med att samhället bygger upp nya, bättre alternativ.