from feedgen.feed import FeedGenerator
fg = FeedGenerator()
fg.load_extension('podcast')
# ...
fg.podcast.itunes_category('Politics')
fg.id('https://vpartiprogrampodden.bitar.se')
fg.title('Vänsterpartiets partiprogram')
fg.description('Vänsterpartiets partiprogram som podd')
fg.author( {'name':'Samuel Skånberg','email':'samuel@bitar.se'} )
fg.logo('https://vpartiprogrampodden.bitar.se/logo.png')
fg.link( href='https://vpartiprogrampodden.bitar.se', rel='alternate' )
fg.language('sv')
# ...
episodes = [
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/01-Inledning.mp3",
        "title": "01-Inledning",
        "description": "Inledningen"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/02-Arbete%20och%20gemenskap.mp3",
        "title": "02-Arbete och gemenskap",
        "description": "Arbete och gemenskap"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/03-Frihetens%20hinder%20-%20Klassamh%C3%A4llet%20och%20kapitalet.mp3",
        "title": "03-Frihetens hinder - Klassamhället och kapitalet",
        "description": "Frihetens hinder - Klassamhället och kapitalet"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/04-Frihetens%20hinder%20-%20Kvinnors%20underordning.mp3",
        "title": "04-Frihetens hinder - Kvinnors underordning",
        "description": "Frihetens hinder - Kvinnors underordning"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/05-Frihetens%20hinder%20-%20Ekologiska%20kriser.mp3",
        "title": "05-Frihetens hinder - Ekologiska kriser",
        "description": "Frihetens hinder - Ekologiska kriser"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/06-Frihetens%20hinder%20-%20En%20oj%C3%A4mlik%20v%C3%A4rld.mp3",
        "title": "06-Frihetens hinder - En ojämlik värld",
        "description": "Frihetens hinder - En ojämlik värld"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/07-Frihetens%20hinder%20-%20Rasism.mp3",
        "title": "07-Frihetens hinder - Rasism",
        "description": "Frihetens hinder - Rasism"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/08-Frihetens%20hinder%20-%20Auktorit%C3%A4ra%20h%C3%B6gerkrafter.mp3",
        "title": "08-Frihetens hinder - Auktoritära högerkrafter",
        "description": "Frihetens hinder - Auktoritära högerkrafter"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/09-V%C3%A5ra%20svar%20-%20Sammanh%C3%A5llning.mp3",
        "title": "09-Våra svar - Sammanhållning",
        "description": "Våra svar - Sammanhållning"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/10-V%C3%A5ra%20svar%20-%20Strategiska%20reformer.mp3",
        "title": "10-Våra svar - Strategiska reformer",
        "description": "Våra svar - Strategiska reformer"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/11-V%C3%A4nsterpartiets%20grundsyn%20i%20sakfr%C3%A5gor%20-%20St%C3%A4rk%20den%20politiska%20demokratin.mp3",
        "title": "11-Vänsterpartiets grundsyn i sakfrågor - Stärk den politiska demokratin",
        "description": "Vänsterpartiets grundsyn i sakfrågor - Stärk den politiska demokratin"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/12-V%C3%A4nsterpartiets%20grundsyn%20i%20sakfr%C3%A5gor%20-%20%C3%84gande%20med%20helhetssyn.mp3",
        "title": "12-Vänsterpartiets grundsyn i sakfrågor - Ägande med helhetssyn",
        "description": "Vänsterpartiets grundsyn i sakfrågor - Ägande med helhetssyn"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/13-V%C3%A4nsterpartiets%20grundsyn%20i%20sakfr%C3%A5gor%20-%20V%C3%A4lfungerande%20arbetsplatser.mp3",
        "title": "13-Vänsterpartiets grundsyn i sakfrågor - Välfungerande arbetsplatser",
        "description": "Vänsterpartiets grundsyn i sakfrågor - Välfungerande arbetsplatser"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/14-V%C3%A4nsterpartiets%20grundsyn%20i%20sakfr%C3%A5gor%20-%20Stora%20investeringar.mp3",
        "title": "14-Vänsterpartiets grundsyn i sakfrågor - Stora investeringar",
        "description": "Vänsterpartiets grundsyn i sakfrågor - Stora investeringar"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/15-V%C3%A4nsterpartiets%20grundsyn%20i%20sakfr%C3%A5gor%20-%20Starka%20f%C3%B6rs%C3%A4kringar.mp3",
        "title": "15-Vänsterpartiets grundsyn i sakfrågor - Starka försäkringar",
        "description": "Vänsterpartiets grundsyn i sakfrågor - Starka försäkringar"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/16-V%C3%A4nsterpartiets%20grundsyn%20i%20sakfr%C3%A5gor%20-%20V%C3%A4lf%C3%A4rdens%20verksamheter.mp3",
        "title": "16-Vänsterpartiets grundsyn i sakfrågor - Välfärdens verksamheter",
        "description": "Vänsterpartiets grundsyn i sakfrågor - Välfärdens verksamheter"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/17-V%C3%A4nsterpartiets%20grundsyn%20i%20sakfr%C3%A5gor%20-%20Utbildning.mp3",
        "title": "17-Vänsterpartiets grundsyn i sakfrågor - Utbildning",
        "description": "Vänsterpartiets grundsyn i sakfrågor - Utbildning"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/18-V%C3%A4nsterpartiets%20grundsyn%20i%20sakfr%C3%A5gor%20-%20V%C3%A5rd%20och%20omsorg.mp3",
        "title": "18-Vänsterpartiets grundsyn i sakfrågor - Vård och omsorg",
        "description": "Vänsterpartiets grundsyn i sakfrågor - Vård och omsorg"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/19-V%C3%A4nsterpartiets%20grundsyn%20i%20sakfr%C3%A5gor%20-%20Seri%C3%B6st%20f%C3%B6retagande.mp3",
        "title": "19-Vänsterpartiets grundsyn i sakfrågor - Seriöst företagande",
        "description": "Vänsterpartiets grundsyn i sakfrågor - Seriöst företagande"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/20-V%C3%A4nsterpartiets%20grundsyn%20i%20sakfr%C3%A5gor%20-%20Infrastruktur%20och%20bost%C3%A4der.mp3",
        "title": "20-Vänsterpartiets grundsyn i sakfrågor - Infrastruktur och bostäder",
        "description": "Vänsterpartiets grundsyn i sakfrågor - Infrastruktur och bostäder"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/21-V%C3%A4nsterpartiets%20grundsyn%20i%20sakfr%C3%A5gor%20-%20Utveckla%20tekniken.mp3",
        "title": "21-Vänsterpartiets grundsyn i sakfrågor - Utveckla tekniken",
        "description": "Vänsterpartiets grundsyn i sakfrågor - Utveckla tekniken"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/22-V%C3%A4nsterpartiets%20grundsyn%20i%20sakfr%C3%A5gor%20-%20Kultur,%20media%20och%20f%C3%B6reningsliv.mp3",
        "title": "22-Vänsterpartiets grundsyn i sakfrågor - Kultur, media och föreningsliv",
        "description": "Vänsterpartiets grundsyn i sakfrågor - Kultur, media och föreningsliv"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/23-V%C3%A4nsterpartiets%20grundsyn%20i%20sakfr%C3%A5gor%20-%20Alla%20m%C3%A4nniskors%20lika%20v%C3%A4rde.mp3",
        "title": "23-Vänsterpartiets grundsyn i sakfrågor - Alla människors lika värde",
        "description": "Vänsterpartiets grundsyn i sakfrågor - Alla människors lika värde"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/24-V%C3%A4nsterpartiets%20grundsyn%20i%20sakfr%C3%A5gor%20-%20Brottsbek%C3%A4mpning.mp3",
        "title": "24-Vänsterpartiets grundsyn i sakfrågor - Brottsbekämpning",
        "description": "Vänsterpartiets grundsyn i sakfrågor - Brottsbekämpning"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/25-V%C3%A4nsterpartiets%20grundsyn%20i%20sakfr%C3%A5gor%20-%20Internationella%20relationer.mp3",
        "title": "25-Vänsterpartiets grundsyn i sakfrågor - Internationella relationer",
        "description": "Vänsterpartiets grundsyn i sakfrågor - Internationella relationer"
    },
    {
        "link": "https://vpartiprogrampodden.bitar.se/partiprogramaudio/25-V%C3%A4nsterpartiets%20grundsyn%20i%20sakfr%C3%A5gor%20-%20Internationella%20relationer.mp3",
        "title": "26-Vänsterpartiets grundsyn i sakfrågor - Vänsterpartiet",
        "description": "Vänsterpartiets grundsyn i sakfrågor - Vänsterpartiet"
    }
]

for episode in episodes:
    fe = fg.add_entry()
    fe.id(episode["link"])
    fe.title(episode["title"])
    fe.description(episode["description"])
    fe.enclosure(episode["link"], 0, 'audio/mpeg')

fg.rss_str(pretty=True)
fg.rss_file('rss.xml')